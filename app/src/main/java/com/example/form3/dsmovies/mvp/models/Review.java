package com.example.form3.dsmovies.mvp.models;

/* Class for Review type */
public class Review {
    private String mId, mAuthor, mReview;

    public Review(String mAuthor, String mReview) {
        this.mAuthor = mAuthor;
        this.mReview = mReview;
    }

    public String getId() {
        return mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public void setAuthor(String author) {
        this.mAuthor = author;
    }

    public String getReview() {
        return mReview;
    }

    public void setReview(String review) {
        this.mReview = review;
    }
}