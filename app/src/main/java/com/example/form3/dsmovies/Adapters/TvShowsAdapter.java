package com.example.form3.dsmovies.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.form3.dsmovies.Data.Settings;
import com.example.form3.dsmovies.Data.Utilities;
import com.example.form3.dsmovies.R;
import com.example.form3.dsmovies.mvp.models.TvShows;

import java.util.List;

/**
 * Created by Daniel Sobral on 03/04/2019.
 */
public class TvShowsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<TvShows> tvShowsList;
    private ImageActions imageActions;

    public TvShowsAdapter(List<TvShows> tvShowsList, ImageActions imageActions) {
        this.tvShowsList = tvShowsList;
        this.imageActions = imageActions;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_program, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final TvShows tvShows;
        final String urlImg, id;
        final ImageView imageView, favImageView;

        tvShows = tvShowsList.get(position);
        urlImg = Settings.IMG_BASE_URL + Settings.IMG_NORMAL_SIZE + tvShows.getThumbnail();
        id = tvShows.getId();
        imageView = ((ViewHolder) holder).mImageView;
        favImageView = ((ViewHolder) holder).mFavoriteImageView;

        Utilities.picassoSetImage(urlImg, imageView);
        imageActions.checkFavorite(id, favImageView);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageActions.onImageClick(id);
            }
        });

        ((ViewHolder) holder).mTitleTextView.setText(tvShows.getTitle());
        ((ViewHolder) holder).mSynopsisTextView.setText(tvShows.getSynopsis());
        favImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageActions.actionFavorite(tvShows, favImageView);
            }
        });

    }

    @Override
    public int getItemCount() {
        return tvShowsList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView mImageView, mFavoriteImageView;
        private TextView mTitleTextView, mSynopsisTextView;

        private ViewHolder(View itemView) {
            super(itemView);

            mImageView = itemView.findViewById(R.id.iv_item_movie_image);
            mFavoriteImageView = itemView.findViewById(R.id.iv_item_movie_favorites);
            mTitleTextView = itemView.findViewById(R.id.tv_item_movie_title);
            mSynopsisTextView = itemView.findViewById(R.id.tv_item_movie_synopsis);
        }
    }

    public interface ImageActions {
        void onImageClick(String id);
        void checkFavorite (String id, ImageView fav);
        void actionFavorite (TvShows tvShows, ImageView fav);
    }
}