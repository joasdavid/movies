package com.example.form3.dsmovies.mvp.contracts.movies.list;

import com.example.form3.dsmovies.mvp.contracts.BasePresenterContract;
import com.example.form3.dsmovies.mvp.models.Movie;

import java.util.List;

/**
 * Created by Daniel Sobral on 26/03/2019.
 */

public interface MoviesListContract {

    interface MoviesListContractView {
        void onSuccess(List<Movie> successResponse);
        void onFail(Exception e);

        // TODO: 02/04/2019 (3) Duplicação das interfaces
        void favouriteState(boolean isFavorite);
    }

    interface MoviesListContractPresenter extends BasePresenterContract<MoviesListContractView> {
        void getListOfMoviesByType(String type);

        // TODO: 02/04/2019 (4) Duplicação das interfaces
        void checkFavoriteList(String id);
        void actionFavorite(Movie movie);
    }
}
