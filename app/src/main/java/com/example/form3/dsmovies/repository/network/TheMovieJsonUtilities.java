package com.example.form3.dsmovies.repository.network;

import com.example.form3.dsmovies.mvp.models.Movie;
import com.example.form3.dsmovies.mvp.models.Review;
import com.example.form3.dsmovies.mvp.models.Trailer;
import com.example.form3.dsmovies.mvp.models.TvShows;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TheMovieJsonUtilities {

    private static final String TMDB_ID = "id";
    private static final String TMDB_IMG = "poster_path";
    private static final String TMDB_RESULTS = "results";
    private static final String TMDB_TITLE = "title";
    private static final String TMDB_NAME = "name";
    private static final String TMDB_ORIGINAL_TITLE = "original_title";
    private static final String TMDB_ORIGINAL_NAME = "original_name";
    private static final String TMDB_SYNOPSIS = "overview";
    private static final String TMDB_TIME = "runtime";
    private static final String TMDB_DATE = "release_date";
    private static final String TMDB_START_DATE = "first_air_date";
    private static final String TMDB_END_DATE = "last_air_date";
    private static final String TMDB_RATE = "vote_average";
    private static final String TMDB_KEY = "key";
    private static final String TMDB_AUTHOR = "author";
    private static final String TMDB_REVIEW = "content";


    public static List<Movie> getMoviesListFromJson(String movieStr)
            throws JSONException {

        List<Movie> parsedMovieData;

        JSONObject movieJson = new JSONObject(movieStr);

        // Is there an error?
        if (!movieJson.has(TMDB_RESULTS)) {
            return null;
        }

        JSONArray movieArray = movieJson.getJSONArray(TMDB_RESULTS);
        parsedMovieData = new ArrayList<>();

        for (int i = 0; i < movieArray.length(); i++) {
            String id, title, img, synopsis;

            JSONObject resultObject = movieArray.getJSONObject(i);

            // Get id and image from movie
            id = resultObject.getString(TMDB_ID);
            title = resultObject.getString(TMDB_TITLE);
            img = resultObject.getString(TMDB_IMG);
            synopsis = resultObject.getString(TMDB_SYNOPSIS);

            parsedMovieData.add(new Movie(id, title, img, synopsis));
        }

        return parsedMovieData;
    }


    public static Movie getMovieDetailsFromJson(String movieDetailsStr, String movieVideosStr, String movieReviewsStr)
            throws JSONException {



        List<Review> reviews = new ArrayList<>();
        List<Trailer> trailer = new ArrayList<>();

        JSONObject movieDetailsJson = new JSONObject(movieDetailsStr);
        JSONObject movieVideosJson = new JSONObject(movieVideosStr);
        JSONObject movieReviewsJson = new JSONObject(movieReviewsStr);

        // Is there an error?
        if (!movieDetailsJson.has(TMDB_ID)) {
            return null;
        }

        String id, title, date, time, rate, img, synopsis;

        id = movieDetailsJson.getString(TMDB_ID);
        title = movieDetailsJson.getString(TMDB_ORIGINAL_TITLE);
        date = movieDetailsJson.getString(TMDB_DATE);
        time = movieDetailsJson.getString(TMDB_TIME);
        rate = movieDetailsJson.getString(TMDB_RATE);
        img = movieDetailsJson.getString(TMDB_IMG);
        synopsis = movieDetailsJson.getString(TMDB_SYNOPSIS);

        if (movieVideosJson.has(TMDB_RESULTS)) {
            JSONArray movieVideosArray = movieVideosJson.getJSONArray(TMDB_RESULTS);

            for (int i = 0; i < movieVideosArray.length(); i++) {
                JSONObject resultObject = movieVideosArray.getJSONObject(i);

                String name, key;
                // Get Trailers
                name = resultObject.getString(TMDB_NAME);
                key = resultObject.getString(TMDB_KEY);

                trailer.add(new Trailer(name, key));
            }
        }

        if (movieReviewsJson.has(TMDB_RESULTS)) {
            JSONArray movieReviewsArray = movieReviewsJson.getJSONArray(TMDB_RESULTS);

            for (int i = 0; i < movieReviewsArray.length(); i++) {
                JSONObject resultObject = movieReviewsArray.getJSONObject(i);

                String author, content;

                author = resultObject.getString(TMDB_AUTHOR);
                content = resultObject.getString(TMDB_REVIEW);
                // Get Reviews

                reviews.add(new Review(author, content));
            }
        }

        return new Movie(id, title, rate, img, synopsis, trailer, reviews, date, time);
    }


    public static List<TvShows> getTvShowsListFromJson(String tvShowStr)
            throws JSONException {

        List<TvShows> parsedTvShowsData;

        JSONObject tvShowJson = new JSONObject(tvShowStr);

        // Is there an error?
        if (!tvShowJson.has(TMDB_RESULTS)) {
            return null;
        }

        JSONArray tvShowsArray = tvShowJson.getJSONArray(TMDB_RESULTS);
        parsedTvShowsData = new ArrayList<>();

        for (int i = 0; i < tvShowsArray.length(); i++) {
            String id, title, img, synopsis;

            JSONObject resultObject = tvShowsArray.getJSONObject(i);

            // Get id and image from movie
            id = resultObject.getString(TMDB_ID);
            title = resultObject.getString(TMDB_NAME);
            img = resultObject.getString(TMDB_IMG);
            synopsis = resultObject.getString(TMDB_SYNOPSIS);

            parsedTvShowsData.add(new TvShows(id, title, img, synopsis));
        }

        return parsedTvShowsData;
    }

    public static TvShows getTvShowsDetailsFromJson(String tvShowDetailsStr, String tvShowVideosStr, String tvShowReviewsStr)
            throws JSONException {

        List<Review> reviews = new ArrayList<>();
        List<Trailer> trailer = new ArrayList<>();

        JSONObject tvShowDetailsJson = new JSONObject(tvShowDetailsStr);
        JSONObject tvShowVideosJson = new JSONObject(tvShowVideosStr);
        JSONObject tvShowReviewsJson = new JSONObject(tvShowReviewsStr);

        // Is there an error?
        if (!tvShowDetailsJson.has(TMDB_ID)) {
            return null;
        }

        String id, title, rate, img, synopsis, start_date, end_date;

        id = tvShowDetailsJson.getString(TMDB_ID);
        title = tvShowDetailsJson.getString(TMDB_ORIGINAL_NAME);
        rate = tvShowDetailsJson.getString(TMDB_RATE);
        img = tvShowDetailsJson.getString(TMDB_IMG);
        synopsis = tvShowDetailsJson.getString(TMDB_SYNOPSIS);
        start_date = tvShowDetailsJson.getString(TMDB_START_DATE);
        end_date = tvShowDetailsJson.getString(TMDB_END_DATE);


        if (tvShowVideosJson.has(TMDB_RESULTS)) {
            JSONArray tvShowVideosArray = tvShowVideosJson.getJSONArray(TMDB_RESULTS);

            for (int i = 0; i < tvShowVideosArray.length(); i++) {
                JSONObject resultObject = tvShowVideosArray.getJSONObject(i);

                String name, key;
                // Get Trailers
                name = resultObject.getString(TMDB_NAME);
                key = resultObject.getString(TMDB_KEY);

                trailer.add(new Trailer(name, key));
            }
        }

        if (tvShowReviewsJson.has(TMDB_RESULTS)) {
            JSONArray tvShowReviewsArray = tvShowReviewsJson.getJSONArray(TMDB_RESULTS);

            for (int i = 0; i < tvShowReviewsArray.length(); i++) {
                JSONObject resultObject = tvShowReviewsArray.getJSONObject(i);

                String author, content;

                author = resultObject.getString(TMDB_AUTHOR);
                content = resultObject.getString(TMDB_REVIEW);
                // Get Reviews

                reviews.add(new Review(author, content));
            }
        }
        return new TvShows(id, title, rate, img, synopsis, trailer, reviews, start_date, end_date);
    }
}