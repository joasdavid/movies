package com.example.form3.dsmovies.mvp.contracts.tvshows.details;

import com.example.form3.dsmovies.mvp.contracts.BasePresenterContract;
import com.example.form3.dsmovies.mvp.models.TvShows;

/**
 * Created by Daniel Sobral on 03/04/2019.
 */
public interface TvShowsDetailsContract {

    interface TvShowsDetailsContractView {

        void onSuccess(TvShows successResponse);

        void onFail(Exception e);

        void favouriteState(boolean isFavorite);
    }

    interface TvShowsDetailsContractPresenter extends BasePresenterContract<TvShowsDetailsContractView> {

        void getTvShowsDetails(String type);

        void checkFavoriteList(String id);

        void actionFavorite(TvShows movie);
    }
}
