package com.example.form3.dsmovies.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.form3.dsmovies.Adapters.MovieAdapter;
import com.example.form3.dsmovies.Adapters.TvShowsAdapter;
import com.example.form3.dsmovies.Data.Settings;
import com.example.form3.dsmovies.Data.Utilities;
import com.example.form3.dsmovies.MoviesApp;
import com.example.form3.dsmovies.R;
import com.example.form3.dsmovies.mvp.contracts.favorites.FavoritesContract.FavoritesContractView;
import com.example.form3.dsmovies.mvp.contracts.favorites.FavoritesPresenter;
import com.example.form3.dsmovies.mvp.models.Movie;
import com.example.form3.dsmovies.mvp.models.TvShows;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Daniel Sobral on 04/04/2019.
 */
public class FavoritesFragment extends BaseFragment implements FavoritesContractView {

    @BindView(R.id.rv_movie_favorite)
    RecyclerView movieRecyclerView;
    @BindView(R.id.rv_tv_show_favorite)
    RecyclerView tvshowecyclerView;
    @BindView(R.id.tv_favorites_movies_title)
    TextView moviesTitleTextView;
    @BindView(R.id.tv_favorites_tv_shows_title)
    TextView tvShowsTitleTextView;

    private static final int SPAN_COUNT_RECYCLER_VIEW = 2;
    public static String TAG = FavoritesFragment.class.getName();

    private View rootView;
    private FavoritesPresenter presenter;

    private ImageView favImageView;
    private FragmentTransaction fragmentTransaction;

    public static String TYPEKEY = "TYPEKEY";
    private String type;

    public static FavoritesFragment newInstance(String type) {

        Bundle args = new Bundle();

        args.putString(TYPEKEY, type);

        FavoritesFragment fragment = new FavoritesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        rootView = getActivity().getLayoutInflater()
                .inflate(R.layout.fragment_favorites, container, false);

        initVarWithArgumentsFromBundle();
        initPresenter();
        initUi();

        return rootView;
    }

    private void initUi() {
        ButterKnife.bind(this, rootView);
        movieRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), SPAN_COUNT_RECYCLER_VIEW));
        tvshowecyclerView.setLayoutManager(new GridLayoutManager(getContext(), SPAN_COUNT_RECYCLER_VIEW));
    }

    private void initPresenter() {
        presenter = new FavoritesPresenter(MoviesApp.getInstance().getMoviesRepo(), MoviesApp.getInstance().getTvShowsRepo());
        presenter.bindView(this);
        showLoading();
        presenter.getFavoritesListByType(type);
    }

    private void initVarWithArgumentsFromBundle() {
        type = getArguments().getString(TYPEKEY);
    }

    @Override
    public void onSuccess(List<Movie> movieList, List<TvShows> tvShowsList) {
        hideLoading();
        if (movieList != null || tvShowsList != null) {
            if (movieList.size() > 0) {
                MovieAdapter movieAdapter = new MovieAdapter(movieList, imageMovieClick());
                movieRecyclerView.setAdapter(movieAdapter);
                moviesTitleTextView.setVisibility(View.VISIBLE);
                movieRecyclerView.setVisibility(View.VISIBLE);
            }
            if (tvShowsList.size() > 0) {
                TvShowsAdapter tvShowsAdapter = new TvShowsAdapter(tvShowsList, imageTvShowClick());
                tvshowecyclerView.setAdapter(tvShowsAdapter);
                tvShowsTitleTextView.setVisibility(View.VISIBLE);
                tvshowecyclerView.setVisibility(View.VISIBLE);
            }
        } else {
            sendToast("Can not get data, try again later!");
        }
    }

    @Override
    public void onFail(Exception e) {
        hideLoading();
        sendToast("Ups something is broken :s");
    }

    @Override
    public void favouriteState(boolean isFavorite) {
        if (isFavorite) {
            favImageView.setImageResource(R.mipmap.star_del);
        } else {
            favImageView.setImageResource(R.mipmap.star_add);
        }
    }

    private void sendToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    private MovieAdapter.ImageActions imageMovieClick() {
        return new MovieAdapter.ImageActions() {
            @Override
            public void onImageClick(Movie movie, ImageView fav) {
                Fragment frag = MovieDetatilsFragment.newInstance(movie.getId());
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                Utilities.changeFragmentWithAnimation(frag,fragmentManager);
            }

            @Override
            public void checkFavorite(String id, ImageView fav) {
                favImageView = fav;
                presenter.checkFavoriteList(id, Settings.PROGRAM_MOVIE);
            }

            @Override
            public void actionFavorite(Movie movie, ImageView fav) {
                favImageView = fav;
                presenter.actionFavorite(movie);
            }
        };
    }

    private TvShowsAdapter.ImageActions imageTvShowClick() {
        return new TvShowsAdapter.ImageActions() {
            @Override
            public void onImageClick(String id) {
                Fragment frag = TvShowsDetailsFragment.newInstance(id);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                Utilities.changeFragmentWithAnimation(frag,fragmentManager);
            }

            @Override
            public void checkFavorite(String id, ImageView fav) {
                favImageView = fav;
                presenter.checkFavoriteList(id, Settings.PROGRAM_TVSHOW);
            }

            @Override
            public void actionFavorite(TvShows tvShows, ImageView fav) {
                favImageView = fav;
                presenter.actionFavorite(tvShows);
            }

        };
    }
}
