package com.example.form3.dsmovies.Adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.form3.dsmovies.Data.Settings;
import com.example.form3.dsmovies.Data.Utilities;
import com.example.form3.dsmovies.R;
import com.example.form3.dsmovies.mvp.models.Movie;

import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Movie> mMovie;
    private ImageActions imageActions;

    public MovieAdapter(List<Movie> movie, ImageActions imageActions) {
        this.mMovie = movie;
        this.imageActions = imageActions;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_program, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        //holder.setIsRecyclable(false);

        final Movie movie;
        final String urlImg, id;
        final ImageView imageView, favImageView;

        movie = mMovie.get(position);
        urlImg = Settings.IMG_BASE_URL + Settings.IMG_NORMAL_SIZE + movie.getThumbnail();
        id = movie.getId();
        imageView = ((ViewHolder) holder).mImageView;
        favImageView = ((ViewHolder) holder).mFavoriteImageView;

        Utilities.picassoSetImage(urlImg, imageView);
        imageActions.checkFavorite(id, favImageView);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageActions.onImageClick(movie, favImageView);
            }
        });

        ((ViewHolder) holder).mTitleTextView.setText(movie.getTitle());
        ((ViewHolder) holder).mSynopsisTextView.setText(movie.getSynopsis());
        favImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageActions.actionFavorite(movie, favImageView);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mMovie.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView mImageView, mFavoriteImageView;
        private TextView mTitleTextView, mSynopsisTextView;

        private ViewHolder(View itemView) {
            super(itemView);

            mImageView = itemView.findViewById(R.id.iv_item_movie_image);
            mFavoriteImageView = itemView.findViewById(R.id.iv_item_movie_favorites);
            mTitleTextView = itemView.findViewById(R.id.tv_item_movie_title);
            mSynopsisTextView = itemView.findViewById(R.id.tv_item_movie_synopsis);
        }
    }

    public interface ImageActions {
        void onImageClick(Movie movie, ImageView fav);
        void checkFavorite (String id, ImageView fav);
        void actionFavorite (Movie movie, ImageView fav);
    }
}