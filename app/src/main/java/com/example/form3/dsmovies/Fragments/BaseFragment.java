package com.example.form3.dsmovies.Fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Joás V. Pereira
 * on 28 Mar. 2019.
 */
public class BaseFragment extends Fragment {

  private ProgressDialog mPDialog;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    mPDialog = new ProgressDialog(getContext());
    return super.onCreateView(inflater, container, savedInstanceState);
  }

  public void showLoading() {
    mPDialog.setMessage("Please Wait!");
    mPDialog.setCancelable(false);
    mPDialog.show();
  }

  public void hideLoading() {
    if (mPDialog.isShowing()) {
      mPDialog.dismiss();
    }
  }

}
