package com.example.form3.dsmovies.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.form3.dsmovies.Data.Settings;
import com.example.form3.dsmovies.R;
import com.example.form3.dsmovies.mvp.models.Trailer;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

import java.util.List;

public class TrailerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Trailer> mTrailer;
    private ThumbnailActions thumbnailActions;

    public TrailerAdapter(List<Trailer> trailers, ThumbnailActions thumbnailActions) {
        this.mTrailer = trailers;
        this.thumbnailActions = thumbnailActions;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder viewHolder;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.trailers_list_item, parent, false);
        viewHolder = new TrailersAdapterViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final String key = mTrailer.get(position).getKey();

        // Get Thumbnail
        ((TrailersAdapterViewHolder) holder).mThumbnailYoutubeView.initialize(Settings.YOUTUBE_API_KEY, new YouTubeThumbnailView.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, final YouTubeThumbnailLoader youTubeThumbnailLoader) {
                youTubeThumbnailLoader.setVideo(key);
                youTubeThumbnailLoader.setOnThumbnailLoadedListener(new YouTubeThumbnailLoader.OnThumbnailLoadedListener() {
                    @Override
                    public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView, String s) {
                        youTubeThumbnailLoader.release();
                    }

                    @Override
                    public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader.ErrorReason errorReason) {

                    }
                });
            }

            @Override
            public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {

            }
        });

        // Click on Thumbnail for open Video
        ((TrailersAdapterViewHolder) holder).mListLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                thumbnailActions.onThumbnailClick(key);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mTrailer.size();
    }

    public class TrailersAdapterViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mListLinearLayout;
        YouTubeThumbnailView mThumbnailYoutubeView;

        public TrailersAdapterViewHolder(View itemView) {
            super(itemView);

            // Trailers Layout
            mThumbnailYoutubeView = (YouTubeThumbnailView) itemView.findViewById(R.id.youtube_thumbnail);
            mListLinearLayout = (LinearLayout) itemView.findViewById(R.id.ll_trailer_list);
        }
    }

    public interface ThumbnailActions {
        void onThumbnailClick(String key);
    }
}