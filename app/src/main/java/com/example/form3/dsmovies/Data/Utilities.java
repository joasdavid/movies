package com.example.form3.dsmovies.Data;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.widget.ImageView;

import com.example.form3.dsmovies.MoviesApp;
import com.example.form3.dsmovies.R;
import com.squareup.picasso.Picasso;

/**
 * Created by Daniel Sobral on 02/04/2019.
 */
public class Utilities {

    public static void picassoSetImage(String url, ImageView imageView) {
        Picasso.with(MoviesApp.getInstance())
                .load(url)
                .into(imageView);
    }

    public static void changeFragmentWithAnimation(Fragment frag, FragmentManager fragmentManager){
        fragmentManager.beginTransaction()
            .setCustomAnimations(android.R.anim.slide_in_left,android.R.anim.slide_out_right, android.R.anim.fade_in,android.R.anim.fade_out)
            .replace(R.id.fragment_main_container, frag, frag.getClass().getName())
            .commit();
    }

    public static void changeFragmentBackStackWithAnimation(Fragment frag, FragmentManager fragmentManager, String backStack){
        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left,android.R.anim.slide_out_right, android.R.anim.fade_in,android.R.anim.fade_out)
                .replace(R.id.fragment_main_container, frag, frag.getClass().getName())
                .addToBackStack(backStack)
                .commit();
    }
}
