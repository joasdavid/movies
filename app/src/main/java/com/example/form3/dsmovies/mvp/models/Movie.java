package com.example.form3.dsmovies.mvp.models;

import java.util.List;

/* Class Movie Type */
public class Movie extends Program{


    private String year, runTime;


    public Movie(String id, String title, String thumbnail, String synopsis) {
        super(id, title, thumbnail, synopsis);
    }

    public Movie(String id, String title, String rate, String thumbnail, String synopsis, List<Trailer> trailer, List<Review> reviews, String year, String runTime) {
        super(id, title, rate, thumbnail, synopsis, trailer, reviews);
        this.year = year;
        this.runTime = runTime;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getRunTime() {
        return runTime;
    }

    public void setRunTime(String runTime) {
        this.runTime = runTime;
    }
}