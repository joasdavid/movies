package com.example.form3.dsmovies.mvp.contracts.tvshows.details;

import com.example.form3.dsmovies.mvp.base.BasePresenter;
import com.example.form3.dsmovies.mvp.contracts.tvshows.details.TvShowsDetailsContract.TvShowsDetailsContractPresenter;
import com.example.form3.dsmovies.mvp.contracts.tvshows.details.TvShowsDetailsContract.TvShowsDetailsContractView;
import com.example.form3.dsmovies.mvp.models.TvShows;
import com.example.form3.dsmovies.repository.FetchDataTask.FetchWorkStates;
import com.example.form3.dsmovies.repository.TvShowsRepo;

/**
 * Created by Daniel Sobral on 03/04/2019.
 */
public class TvShowsDetailsPresenter extends BasePresenter<TvShowsDetailsContractView>
        implements TvShowsDetailsContractPresenter, FetchWorkStates<TvShows> {

    TvShowsRepo tvShowsRepo;

    public TvShowsDetailsPresenter(TvShowsRepo tvShowsRepo) {
        this.tvShowsRepo = tvShowsRepo;
    }

    @Override
    public void getTvShowsDetails(String id) {
        tvShowsRepo.getTvShows(id, this);
    }

    @Override
    public void checkFavoriteList(String id) {
        boolean isFavorite;

        isFavorite = tvShowsRepo.isFavorite(id);

       getViewState().favouriteState(isFavorite);
    }

    @Override
    public void actionFavorite(TvShows movie) {
        tvShowsRepo.setFavorite(movie);
        checkFavoriteList(movie.getId());
    }

    @Override
    public void onStartWork() {
        //void
    }

    @Override
    public void onEndWork() {
        //void
    }

    @Override
    public void onSuccess(TvShows tvShows) {
        getViewState().onSuccess(tvShows);
    }

    @Override
    public void onFail(Exception e) {
        getViewState().onFail(e);
    }
}