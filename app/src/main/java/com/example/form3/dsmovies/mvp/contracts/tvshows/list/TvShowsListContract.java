package com.example.form3.dsmovies.mvp.contracts.tvshows.list;

import com.example.form3.dsmovies.mvp.contracts.BasePresenterContract;
import com.example.form3.dsmovies.mvp.models.TvShows;

import java.util.List;

/**
 * Created by Daniel Sobral on 03/04/2019.
 */
public interface TvShowsListContract {
    interface TvShowsListContractView {

        void onSuccess(List<TvShows> successResponse);

        void onFail(Exception e);

        void favouriteState(boolean isFavorite);
    }

    interface TvShowsListContractPresenter extends BasePresenterContract<TvShowsListContractView> {

        void getListOfMoviesByType(String type);

        void checkFavoriteList(String id);

        void actionFavorite(TvShows tvShows);
    }
}
