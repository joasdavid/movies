package com.example.form3.dsmovies.mvp.models;

/* Class for Trailer type */
public class Trailer {
    private String mTitle, mKey;

    public Trailer(String title, String key) {
        this.mTitle = title;
        this.mKey = key;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getKey() {
        return mKey;
    }

    public void setKey(String key) {
        this.mKey = key;
    }
}