package com.example.form3.dsmovies.mvp.base;

import com.example.form3.dsmovies.mvp.contracts.BasePresenterContract;

/**
 * Created by Joás V. Pereira
 * on 28 Mar. 2019.
 */
public class BasePresenter<T> implements BasePresenterContract<T> {

  private T viewState;

  @Override
  public void bindView(T view) {
    viewState = view;
  }

  public T getViewState() {
    return viewState;
  }

  public void setViewState(T viewState) {
    this.viewState = viewState;
  }
}
