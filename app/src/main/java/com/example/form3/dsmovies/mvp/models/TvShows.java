package com.example.form3.dsmovies.mvp.models;

import java.util.List;

/**
 * Created by Daniel Sobral on 03/04/2019.
 */
public class TvShows extends Program {

    private String start_date, finish_date;

    public TvShows(String id, String title, String thumbnail, String synopsis) {
        super(id, title, thumbnail, synopsis);
    }

    public TvShows(String id, String title, String rate, String thumbnail, String synopsis, List<Trailer> trailer, List<Review> review, String start_date, String finish_date) {
        super(id, title, rate, thumbnail, synopsis, trailer, review);
        this.start_date = start_date;
        this.finish_date = finish_date;
    }

    public String getStart_date(){
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getFinish_date() {
        return finish_date;
    }

    public void setFinish_date(String finish_date) {
        this.finish_date = finish_date;
    }
}
