package com.example.form3.dsmovies.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.form3.dsmovies.Adapters.TvShowsAdapter;
import com.example.form3.dsmovies.Adapters.TvShowsAdapter.ImageActions;
import com.example.form3.dsmovies.Data.Utilities;
import com.example.form3.dsmovies.MoviesApp;
import com.example.form3.dsmovies.R;
import com.example.form3.dsmovies.mvp.contracts.tvshows.list.TvShowsListContract.TvShowsListContractView;
import com.example.form3.dsmovies.mvp.contracts.tvshows.list.TvShowsListPresenter;
import com.example.form3.dsmovies.mvp.models.TvShows;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Daniel Sobral on 03/04/2019.
 */

public class TvShowsListFragment extends BaseFragment implements TvShowsListContractView {

    @BindView(R.id.rv_movie_list)
    RecyclerView recyclerView;

    private static final int SPAN_COUNT_RECYCLER_VIEW = 2;
    public static String TAG = TvShowsListFragment.class.getName();

    private View rootView;

    private ImageView favImageView;

    private TvShowsListPresenter presenter;

    public static String TYPEKEY = "TYPEKEY";
    private String type;

    public static TvShowsListFragment newInstance(String type) {

        Bundle args = new Bundle();

        args.putString(TYPEKEY, type);

        TvShowsListFragment fragment = new TvShowsListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        rootView = getActivity().getLayoutInflater()
                .inflate(R.layout.fragment_movies_list, container, false);

        initVarWithArgumentsFromBundle();
        initPresenter();
        initUi();

        return rootView;
    }

    private void initUi() {
        ButterKnife.bind(this, rootView);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), SPAN_COUNT_RECYCLER_VIEW));
    }

    private void initVarWithArgumentsFromBundle() {
        type = getArguments().getString(TYPEKEY);
    }

    private void initPresenter() {
        presenter = new TvShowsListPresenter(MoviesApp.getInstance().getTvShowsRepo());
        presenter.bindView(this);
        showLoading();
        presenter.getListOfMoviesByType(type);
    }

    @Override
    public void onSuccess(List<TvShows> tvShowsList) {
        hideLoading();
        if (tvShowsList != null) {
            TvShowsAdapter tvShowsAdapter = new TvShowsAdapter(tvShowsList, imageClick());
            recyclerView.setAdapter(tvShowsAdapter);
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            Toast.makeText(getContext(), "Can not get data, try again later!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFail(Exception e) {
        hideLoading();
        Toast.makeText(getContext(), "Ups something is broken :s", Toast.LENGTH_LONG).show();
    }

    @Override
    public void favouriteState(boolean isFavorite) {
        if(isFavorite && favImageView != null){
            favImageView.setImageResource(R.mipmap.star_del);
        } else if (!isFavorite && favImageView != null){
            favImageView.setImageResource(R.mipmap.star_add);
        }
    }

    private void sendToast (String message){
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    private TvShowsAdapter.ImageActions imageClick() {
        return new ImageActions() {
            @Override
            public void onImageClick(String id) {
                String backStack = null;
                Fragment frag = TvShowsDetailsFragment.newInstance(id);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                Utilities.changeFragmentBackStackWithAnimation(frag,fragmentManager, backStack);
            }

            @Override
            public void checkFavorite(String id, ImageView fav) {
                favImageView = fav;
                presenter.checkFavoriteList(id);
            }

            @Override
            public void actionFavorite(TvShows tvShows, ImageView fav) {
                favImageView = fav;
                presenter.actionFavorite(tvShows);
            }
        };
    }
}