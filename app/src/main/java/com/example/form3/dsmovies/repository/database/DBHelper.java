package com.example.form3.dsmovies.repository.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.form3.dsmovies.mvp.models.Movie;
import com.example.form3.dsmovies.mvp.models.TvShows;

public class DBHelper extends SQLiteOpenHelper {

    private static int version = 1;
    private static String dbName = "favorites.db";
    private static String TABLE_MOVIES = "MOVIES";
    private static String TABLE_TVSHOWS = "TVSHOWS";

    String[] sql = {
            "CREATE TABLE " + TABLE_MOVIES + " (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, id_movie INTEGER " +
                    ", title TEXT, thumbnail TEXT, synopsis TEXT);",
            "CREATE TABLE " + TABLE_TVSHOWS + " (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, id_tvshow INTEGER " +
                    ", title TEXT, thumbnail TEXT, synopsis TEXT);"
    };

    public DBHelper(Context context) {
        super(context, dbName, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (String s : sql) {
            db.execSQL(s);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        version++;
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MOVIES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TVSHOWS);
        onCreate(db);
    }

    /* ### FAVORITES ### */
    // Insert movie on DB


    public long insertFavorite(Movie movie){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id_movie", movie.getId());
        contentValues.put("title", movie.getTitle());
        contentValues.put("thumbnail", movie.getThumbnail());
        contentValues.put("synopsis", movie.getSynopsis());
        return db.insert(TABLE_MOVIES, null, contentValues);
    }

    public long insertFavorite(TvShows tvShows){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id_tvshow", tvShows.getId());
        contentValues.put("title", tvShows.getTitle());
        contentValues.put("thumbnail", tvShows.getThumbnail());
        contentValues.put("synopsis", tvShows.getSynopsis());
        return db.insert(TABLE_TVSHOWS, null, contentValues);
    }

    // Get list of movies in DB
    public Cursor getFavoritesMovieList() {
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("SELECT * FROM " + TABLE_MOVIES, null);
    }

    // Get movie by ID
    public Cursor getMovieFavorite(String id_movie) {
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("SELECT * FROM " + TABLE_MOVIES + " WHERE id_movie = :id_movie", new String[]{id_movie});
    }

    // Delete movie by ID
    public long deleteMovieFavorite(String id_movie) {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(TABLE_MOVIES, "id_movie = :id_movie", new String[]{id_movie});
    }

    public Cursor getFavoritesTvShowsList() {
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("SELECT * FROM " + TABLE_TVSHOWS, null);
    }

    public Cursor getTvShowsFavorite(String id_tvshow) {
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("SELECT * FROM " + TABLE_TVSHOWS + " WHERE id_tvshow = :id_tvshow", new String[]{id_tvshow});
    }

    public long deleteTvShowFavorite(String id_tvshow) {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(TABLE_TVSHOWS, "id_tvshow = :id_tvshow", new String[]{id_tvshow});
    }

    /* ### .FAVORITES ### */
}