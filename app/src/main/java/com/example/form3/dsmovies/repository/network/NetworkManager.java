package com.example.form3.dsmovies.repository.network;

import com.example.form3.dsmovies.mvp.models.Movie;
import com.example.form3.dsmovies.mvp.models.TvShows;

import org.json.JSONException;

import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * Created by Joás V. Pereira
 * on 19 Mar. 2019.
 */
public class NetworkManager {

  private static NetworkManager networkManager;

  @SuppressWarnings("FieldCanBeLocal")
  private final String VIDEOS_TYPE = "videos";
  @SuppressWarnings("FieldCanBeLocal")
  private final String REVIEW_TYPE = "reviews";
  @SuppressWarnings("FieldCanBeLocal")
  private final String URL_DIVIDER = "/";

  public static NetworkManager getInstance(){
    if(networkManager != null){
      return networkManager;
    }
    return new NetworkManager();
  }

  private NetworkManager() {}

  public Movie getMovieDetails(String id) throws IOException, JSONException {

    URL movieRequestUrl = NetworkUtilities.buildUrl(id);
    URL movieVideoRequestUrl = NetworkUtilities.buildUrl(
        String.format("%s%s%s", id, URL_DIVIDER, VIDEOS_TYPE));
    URL movieReviewRequestUrl = NetworkUtilities.buildUrl(
        String.format("%s%s%s", id, URL_DIVIDER, REVIEW_TYPE));

    String jsonMovieDetailsResponse = NetworkUtilities.getResponseFromHttpUrl(movieRequestUrl);
    String jsonMovieVideosResponse = NetworkUtilities.getResponseFromHttpUrl(movieVideoRequestUrl);
    String jsonMovieReviewsResponse = NetworkUtilities
        .getResponseFromHttpUrl(movieReviewRequestUrl);

    return TheMovieJsonUtilities.getMovieDetailsFromJson(jsonMovieDetailsResponse,
        jsonMovieVideosResponse, jsonMovieReviewsResponse);
  }

  public List<Movie> getListMovies(String type) throws IOException, JSONException {
    URL movieRequestUrl = NetworkUtilities.buildUrl(type);
    String jsonMovieResponse = NetworkUtilities.getResponseFromHttpUrl(movieRequestUrl);
    return TheMovieJsonUtilities.getMoviesListFromJson(jsonMovieResponse);
  }

  public TvShows getTvShowsDetails(String id, String program) throws IOException, JSONException {

    URL tvShowsRequestUrl = NetworkUtilities.buildUrl(id, program);
    URL tvShowsVideoRequestUrl = NetworkUtilities.buildUrl(
            String.format("%s%s%s", id, URL_DIVIDER, VIDEOS_TYPE), program);
    URL tvShowsReviewRequestUrl = NetworkUtilities.buildUrl(
            String.format("%s%s%s", id, URL_DIVIDER, REVIEW_TYPE), program);

    String jsonTvShowsDetailsResponse = NetworkUtilities.getResponseFromHttpUrl(tvShowsRequestUrl);
    String jsonTvShowsVideosResponse = NetworkUtilities.getResponseFromHttpUrl(tvShowsVideoRequestUrl);
    String jsonTvShowsReviewsResponse = NetworkUtilities
            .getResponseFromHttpUrl(tvShowsReviewRequestUrl);

    return TheMovieJsonUtilities.getTvShowsDetailsFromJson(jsonTvShowsDetailsResponse,
            jsonTvShowsVideosResponse, jsonTvShowsReviewsResponse);
  }

  public List<TvShows> getTvShowsList(String type, String program) throws IOException, JSONException {
    URL tvShowsRequestUrl = NetworkUtilities.buildUrl(type, program);
    String jsonTvShowsResponse = NetworkUtilities.getResponseFromHttpUrl(tvShowsRequestUrl);
    return TheMovieJsonUtilities.getTvShowsListFromJson(jsonTvShowsResponse);
  }

}
