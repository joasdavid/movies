package com.example.form3.dsmovies.Fragments;

import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.ChangeTransform;
import android.transition.TransitionSet;

/**
 * Created by Joás V. Pereira
 * on 19 Mar. 2019.
 */
public class DetailsTransition extends TransitionSet {
  public DetailsTransition() {
    setOrdering(ORDERING_TOGETHER);
    addTransition(new ChangeBounds()).
        addTransition(new ChangeTransform()).
        addTransition(new ChangeImageTransform());
  }
}