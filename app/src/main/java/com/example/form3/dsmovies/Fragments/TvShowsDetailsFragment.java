package com.example.form3.dsmovies.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.form3.dsmovies.Adapters.ReviewsAdapter;
import com.example.form3.dsmovies.Adapters.TrailerAdapter;
import com.example.form3.dsmovies.Data.Settings;
import com.example.form3.dsmovies.Data.Utilities;
import com.example.form3.dsmovies.MoviesApp;
import com.example.form3.dsmovies.R;
import com.example.form3.dsmovies.YoutubeActivity;
import com.example.form3.dsmovies.mvp.contracts.tvshows.details.TvShowsDetailsContract.TvShowsDetailsContractView;
import com.example.form3.dsmovies.mvp.contracts.tvshows.details.TvShowsDetailsPresenter;
import com.example.form3.dsmovies.mvp.models.TvShows;
import com.example.form3.dsmovies.viewgroup.CustomBarViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Daniel Sobral on 03/04/2019.
 */
public class TvShowsDetailsFragment extends BaseFragment implements TvShowsDetailsContractView {

    public static String TAG = TvShowsDetailsFragment.class.getName();

    @BindView(R.id.tv_title_details) TextView titleTextView;
    @BindView(R.id.iv_thumb_details) ImageView thumbnailImageView;
    @BindView(R.id.tv_start_date_details) TextView startDateTextView;
    @BindView(R.id.tv_end_date_details) TextView endDateTextView;
    @BindView(R.id.tv_rate_details) TextView rateTextView;
    @BindView(R.id.iv_favorites) ImageView favoriteImageView;
    @BindView(R.id.tv_synopsis_details) TextView synopsisTextView;
    @BindView(R.id.rv_trailers) RecyclerView trailersRecyclerView;
    @BindView(R.id.rv_reviews) RecyclerView reviewsRecyclerView;
    @BindView(R.id.cvg_synopsis) CustomBarViewGroup synopsisCustom;
    @BindView(R.id.cvg_trailers) CustomBarViewGroup trailersCustom;
    @BindView(R.id.cvg_reviews) CustomBarViewGroup reviewsCustom;

    private View rootView;
    private TvShows tvShows;

    private TvShowsDetailsPresenter presenter;

    public static String IDKEY = "IDKEY";
    private String id;

    public static TvShowsDetailsFragment newInstance(String id) {

        Bundle args = new Bundle();

        args.putString(IDKEY, id);

        TvShowsDetailsFragment fragment = new TvShowsDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        rootView = getActivity().getLayoutInflater()
                .inflate(R.layout.fragment_tv_shows_details, container, false);

        initVarWithArgumentsFromBundle();
        initPresenter();
        initUi();

        favoriteImageView.setOnClickListener(favoriteActionOnClickListner);

        return rootView;
    }

    private void initUi() {

        ButterKnife.bind(this, rootView);

        trailersRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        reviewsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void initVarWithArgumentsFromBundle() {
        id = getArguments().getString(IDKEY);
    }

    private void initPresenter() {
        presenter = new TvShowsDetailsPresenter(MoviesApp.getInstance().getTvShowsRepo());
        presenter.bindView(this);
        showLoading();
        presenter.getTvShowsDetails(id);
    }

    @Override
    public void onSuccess(TvShows tvShows) {
        hideLoading();
        if (tvShows != null) {
            this.tvShows = tvShows;
            presenter.checkFavoriteList(tvShows.getId());
            titleTextView.setText(tvShows.getTitle());
            String url = Settings.IMG_BASE_URL + Settings.IMG_THUMBNAIL_SIZE + tvShows.getThumbnail();
            Utilities.picassoSetImage(url, thumbnailImageView);
            String start_date = getString(R.string.first_episode) + " " + tvShows.getStart_date();
            startDateTextView.setText(start_date);
            String end_date = getString(R.string.last_episode) + " " + tvShows.getFinish_date();
            endDateTextView.setText(end_date);
            String rate =  getString(R.string.rate) + " " + tvShows.getRate() +  getString(R.string.rate_max);
            rateTextView.setText(rate);


            if (!TextUtils.isEmpty(tvShows.getSynopsis())) {
                synopsisTextView.setText(tvShows.getSynopsis());
                synopsisCustom.setVisibility(View.VISIBLE);
            }

            if (tvShows.getTrailer().size() > 0) {
                TrailerAdapter trailersAdapter = new TrailerAdapter(tvShows.getTrailer(), thumbnailActions);
                trailersRecyclerView.setAdapter(trailersAdapter);
                trailersCustom.setVisibility(View.VISIBLE);

            }

            if (tvShows.getReviews().size() > 0) {
                ReviewsAdapter reviewsAdapter = new ReviewsAdapter(tvShows.getReviews());
                reviewsRecyclerView.setAdapter(reviewsAdapter);
                reviewsCustom.setVisibility(View.VISIBLE);

            }

        } else {
            sendToast("Can not get data, try again later!");
        }
    }

    @Override
    public void onFail(Exception e) {
        hideLoading();
        sendToast("Ups something is broken :s");
    }

    @Override
    public void favouriteState(boolean isFavorite) {
        if(isFavorite) {
            favoriteImageView.setImageResource(R.mipmap.star_del);
        } else {
            favoriteImageView.setImageResource(R.mipmap.star_add);
        }
    }

    private View.OnClickListener favoriteActionOnClickListner = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            presenter.actionFavorite(tvShows);
        }
    };

    private void sendToast (String message){
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    private TrailerAdapter.ThumbnailActions thumbnailActions = new TrailerAdapter.ThumbnailActions() {
        @Override
        public void onThumbnailClick(String key) {
            Intent intent = new Intent(getContext(), YoutubeActivity.class);
            intent.putExtra(Settings.KEY, key);
            startActivity(intent);
        }
    };
}
