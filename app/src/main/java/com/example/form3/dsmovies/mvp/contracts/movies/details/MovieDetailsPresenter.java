package com.example.form3.dsmovies.mvp.contracts.movies.details;

import com.example.form3.dsmovies.mvp.base.BasePresenter;
import com.example.form3.dsmovies.mvp.contracts.movies.details.MovieDetailsContract.MovieDetailsContractPresenter;
import com.example.form3.dsmovies.mvp.contracts.movies.details.MovieDetailsContract.MovieDetailsContractView;
import com.example.form3.dsmovies.mvp.models.Movie;
import com.example.form3.dsmovies.repository.FetchDataTask.FetchWorkStates;
import com.example.form3.dsmovies.repository.MoviesRepo;

/**
 * Created by Daniel Sobral on 28/03/2019.
 */
public class MovieDetailsPresenter extends BasePresenter<MovieDetailsContractView> implements
        MovieDetailsContractPresenter, FetchWorkStates<Movie> {

    MoviesRepo moviesRepo;

    public MovieDetailsPresenter(MoviesRepo moviesRepo) {
        this.moviesRepo = moviesRepo;
    }

    @Override
    public void getMovieDetails(String id) {
        moviesRepo.getMovie(id, this);
    }

    @Override
    public void checkFavoriteList(String id) {
        boolean isFavorite;

        isFavorite = moviesRepo.isFavorite(id);

        getViewState().favouriteState(isFavorite);
    }

    @Override
    public void actionFavorite(Movie movie) {
        moviesRepo.setFavorite(movie);
        checkFavoriteList(movie.getId());
    }

    @Override
    public void onStartWork() {
        //void
    }

    @Override
    public void onEndWork() {
        //void
    }

    @Override
    public void onSuccess(Movie movie) {
        getViewState().onSuccess(movie);
    }

    @Override
    public void onFail(Exception e) {
        getViewState().onFail(e);
    }
}