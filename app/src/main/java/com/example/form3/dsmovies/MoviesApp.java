package com.example.form3.dsmovies;

import android.app.Application;

import com.example.form3.dsmovies.repository.MoviesRepo;
import com.example.form3.dsmovies.repository.TvShowsRepo;
import com.example.form3.dsmovies.repository.database.DBHelper;

/**
 * Created by Joás V. Pereira
 * on 19 Mar. 2019.
 */
public class MoviesApp extends Application {

    private static MoviesApp mobileApplicationClass;

    private MoviesRepo moviesRepo;

    private TvShowsRepo tvShowsRepo;

    public static MoviesApp getInstance() {
        return mobileApplicationClass;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mobileApplicationClass = this;
        moviesRepo = new MoviesRepo(new DBHelper(this));
        tvShowsRepo = new TvShowsRepo(new DBHelper(this));
    }

    public MoviesRepo getMoviesRepo() {
        return moviesRepo;
    }

    public TvShowsRepo getTvShowsRepo (){
        return tvShowsRepo;
    }

}
