package com.example.form3.dsmovies.mvp.contracts.movies.details;

import com.example.form3.dsmovies.mvp.contracts.BasePresenterContract;
import com.example.form3.dsmovies.mvp.models.Movie;

/**
 * Created by Daniel Sobral on 28/03/2019.
 */
public interface MovieDetailsContract {

    interface MovieDetailsContractView {
        void onSuccess(Movie successResponse);
        void onFail(Exception e);

      // COMPLETED: 28/03/2019 Pode ser so um metodo exp: favouriteState(boolean isFavourite)
        void favouriteState(boolean isFavorite);

    }

    interface MovieDetailsContractPresenter extends BasePresenterContract<MovieDetailsContractView> {
        void getMovieDetails(String type);

        void checkFavoriteList(String id);
        void actionFavorite(Movie movie);
    }
}
