package com.example.form3.dsmovies.repository;
import android.os.AsyncTask;

/**
 * Created by Joás V. Pereira
 * on 19 Mar. 2019.
 */
public class FetchDataTask<I, O> extends AsyncTask<I, Void, O> {

  private FetchWork<I, O> fetchWorkInterface;
  private FetchWorkStates<O> workStatesInterface;
  private Exception exception;
  private FetchWorkStates<O> workStates;

  public FetchDataTask(
      FetchWork<I, O> fetchWorkInterface,
      FetchWorkStates<O> workStatesInterface) {
    this.fetchWorkInterface = fetchWorkInterface;
    this.workStatesInterface = workStatesInterface;
  }

  @Override
  protected O doInBackground(I... is) {
    try {
      return fetchWorkInterface.onBackgroundWork(is);
    } catch (Exception e) {
      exception = e;
      return null;
    }
  }

  @Override
  protected void onPreExecute() {
    super.onPreExecute();
    workStatesInterface.onStartWork();
  }

  @Override
  protected void onPostExecute(O o) {
    super.onPostExecute(o);
    workStatesInterface.onEndWork();
    if (exception == null) {
      workStatesInterface.onSuccess(o);
    }else {
      //noinspection ConstantConditions
      workStatesInterface.onFail(exception);
    }
  }


  public interface FetchWork<I, O> {

    O onBackgroundWork(I... parm) throws Exception;
  }

  public interface FetchWorkStates<T> {

    void onStartWork();

    void onEndWork();

    void onSuccess(T t);

    void onFail(Exception e);
  }


}
