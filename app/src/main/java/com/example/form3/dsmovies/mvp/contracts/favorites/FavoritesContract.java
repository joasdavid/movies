package com.example.form3.dsmovies.mvp.contracts.favorites;

import com.example.form3.dsmovies.mvp.contracts.BasePresenterContract;
import com.example.form3.dsmovies.mvp.models.Movie;
import com.example.form3.dsmovies.mvp.models.TvShows;

import java.util.List;

/**
 * Created by Daniel Sobral on 04/04/2019.
 */
public interface FavoritesContract {

    interface FavoritesContractView {
        void onSuccess(List<Movie> movieList, List<TvShows> tvShowsList);

        void onFail(Exception e);

        void favouriteState(boolean isFavorite);
    }

    interface FavoritesContractPresenter extends BasePresenterContract<FavoritesContractView> {
        void getFavoritesListByType(String type);

        void checkFavoriteList(String id, String type);

        void actionFavorite(Movie movie);

        void actionFavorite(TvShows tvShows);
    }
}
