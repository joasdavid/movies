package com.example.form3.dsmovies.mvp.contracts.tvshows.list;

import com.example.form3.dsmovies.mvp.base.BasePresenter;
import com.example.form3.dsmovies.mvp.contracts.tvshows.list.TvShowsListContract.TvShowsListContractPresenter;
import com.example.form3.dsmovies.mvp.contracts.tvshows.list.TvShowsListContract.TvShowsListContractView;
import com.example.form3.dsmovies.mvp.models.TvShows;
import com.example.form3.dsmovies.repository.FetchDataTask.FetchWorkStates;
import com.example.form3.dsmovies.repository.TvShowsRepo;
import com.example.form3.dsmovies.repository.TvShowsRepo.TvShowsType;

import java.util.List;

/**
 * Created by Daniel Sobral on 03/04/2019.
 */
public class TvShowsListPresenter extends BasePresenter<TvShowsListContractView> implements
        TvShowsListContractPresenter, FetchWorkStates<List<TvShows>> {

    TvShowsRepo tvShowsRepo;

    public TvShowsListPresenter(TvShowsRepo tvShowsRepo) {
        this.tvShowsRepo = tvShowsRepo;
    }


    @Override
    public void getListOfMoviesByType(String type) {
        tvShowsRepo.getTvShowsList(TvShowsType.getTypeString(type), this);
    }

    @Override
    public void checkFavoriteList(String id) {
        boolean isFavorite;

        isFavorite = tvShowsRepo.isFavorite(id);

        getViewState().favouriteState(isFavorite);
    }

    @Override
    public void actionFavorite(TvShows tvShows) {
        tvShowsRepo.setFavorite(tvShows);
        checkFavoriteList(tvShows.getId());
    }

    @Override
    public void onStartWork() {

    }

    @Override
    public void onEndWork() {

    }

    @Override
    public void onSuccess(List<TvShows> tvShows) {
        getViewState().onSuccess(tvShows);
    }

    @Override
    public void onFail(Exception e) {
        getViewState().onFail(e);
    }
}
