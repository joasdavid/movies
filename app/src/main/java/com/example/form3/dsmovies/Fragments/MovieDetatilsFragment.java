package com.example.form3.dsmovies.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.form3.dsmovies.Adapters.ReviewsAdapter;
import com.example.form3.dsmovies.Adapters.TrailerAdapter;
import com.example.form3.dsmovies.Data.Settings;
import com.example.form3.dsmovies.Data.Utilities;
import com.example.form3.dsmovies.MoviesApp;
import com.example.form3.dsmovies.R;
import com.example.form3.dsmovies.YoutubeActivity;
import com.example.form3.dsmovies.mvp.contracts.movies.details.MovieDetailsContract.MovieDetailsContractView;
import com.example.form3.dsmovies.mvp.contracts.movies.details.MovieDetailsPresenter;
import com.example.form3.dsmovies.mvp.models.Movie;
import com.example.form3.dsmovies.viewgroup.CustomBarViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MovieDetatilsFragment extends BaseFragment implements MovieDetailsContractView {

    public static String TAG = MovieDetatilsFragment.class.getName();

    @BindView(R.id.tv_title_details) TextView mTitleTextView;
    @BindView(R.id.iv_thumb_details) ImageView mThumbnailImageView;
    @BindView(R.id.tv_year_details) TextView mYearTextView;
    @BindView(R.id.tv_time_details) TextView mRunTimeTextView;
    @BindView(R.id.tv_rate_details) TextView mRateTextView;
    @BindView(R.id.iv_favorites) ImageView mFavoriteImageView;
    @BindView(R.id.tv_synopsis_details) TextView mSynopsisTextView;
    @BindView(R.id.rv_trailers) RecyclerView mTrailersRecyclerView;
    @BindView(R.id.rv_reviews) RecyclerView mReviewsRecyclerView;
    @BindView(R.id.cvg_synopsis) CustomBarViewGroup mSynopsisCustom;
    @BindView(R.id.cvg_trailers) CustomBarViewGroup mTrailersCustom;
    @BindView(R.id.cvg_reviews) CustomBarViewGroup mReviewsCustom;


    private View rootView;
    private Movie movie;

    private MovieDetailsPresenter presenter;

    public static String IDKEY = "IDKEY";
    private String mId;

    public static MovieDetatilsFragment newInstance(String id) {

        Bundle args = new Bundle();

        args.putString(IDKEY, id);

        MovieDetatilsFragment fragment = new MovieDetatilsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        rootView = getActivity().getLayoutInflater()
                .inflate(R.layout.fragment_movies_details, container, false);

        initVarWithArgumentsFromBundle();
        initPresenter();
        initUi();

        // COMPLETED: 28/03/2019 Não é preciso guardar em memoria (numa variavel) o getActivity() pode ser utilizado em todo o fragment
        // COMPLETED: 28/03/2019 não está aqui a fazer nada!

        mFavoriteImageView.setOnClickListener(favoriteActionOnClickListner);

        return rootView;
    }

    private void initUi() {
      // COMPLETED: 28/03/2019 utilizar butter knife
        ButterKnife.bind(this, rootView);

        mTrailersRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        mReviewsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void initVarWithArgumentsFromBundle() {
        mId = getArguments().getString(IDKEY);
    }

    private void initPresenter() {
        presenter = new MovieDetailsPresenter(MoviesApp.getInstance().getMoviesRepo());
        presenter.bindView(this);
        showLoading();
        presenter.getMovieDetails(mId);
    }

    @Override
    public void onSuccess(Movie movie) {
        hideLoading();
        if (movie != null) {
            this.movie = movie;
          // COMPLETED: 28/03/2019 se é sempre chamado quando tens um sucesso pode estar é dentro do presenter
            presenter.checkFavoriteList(movie.getId());
            mTitleTextView.setText(movie.getTitle());
            String url = Settings.IMG_BASE_URL + Settings.IMG_THUMBNAIL_SIZE + movie.getThumbnail();
          // COMPLETED: 28/03/2019 Faz sentido, eu só acho que fica melhor organizado dentro de uma class para Utils em vez de estar na appClass para dividir responsabilidades
            Utilities.picassoSetImage(url, mThumbnailImageView);
            String year = getString(R.string.year) + " " + movie.getYear().substring(0, 4);
            mYearTextView.setText(year);
            String time =  getString(R.string.time) + " " + movie.getRunTime() +  getString(R.string.time_type);
            mRunTimeTextView.setText(time);
            String rate =  getString(R.string.rate) + " " + movie.getRate() +  getString(R.string.rate_max);
            mRateTextView.setText(rate);


            if (!TextUtils.isEmpty(movie.getSynopsis())) {
                //mSynopsisCustom.setTitle(getString(R.string.synopsis));
                mSynopsisTextView.setText(movie.getSynopsis());
                mSynopsisCustom.setVisibility(View.VISIBLE);

            }

            if (movie.getTrailer().size() > 0) {
                //mTrailersCustom.setTitle(getString(R.string.trailers));
                TrailerAdapter mTrailersAdapter = new TrailerAdapter(movie.getTrailer(), thumbnailActions);
                mTrailersRecyclerView.setAdapter(mTrailersAdapter);
                mTrailersCustom.setVisibility(View.VISIBLE);

            }

            if (movie.getReviews().size() > 0) {
                //mReviewsCustom.setTitle(getString(R.string.reviews));
                ReviewsAdapter mReviewsAdapter = new ReviewsAdapter(movie.getReviews());
                mReviewsRecyclerView.setAdapter(mReviewsAdapter);
                mReviewsCustom.setVisibility(View.VISIBLE);

            }

        } else {
            sendToast("Can not get data, try again later!");
        }
    }

    @Override
    public void onFail(Exception e) {
        hideLoading();
        sendToast("Ups something is broken :s");
    }

    @Override
    public void favouriteState(boolean isFavorite) {
        if(isFavorite) {
            mFavoriteImageView.setImageResource(R.mipmap.star_del);
        } else {
            mFavoriteImageView.setImageResource(R.mipmap.star_add);
        }
    }

    private void sendToast (String message){
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    private View.OnClickListener favoriteActionOnClickListner = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            presenter.actionFavorite(movie);
        }
    };

    private TrailerAdapter.ThumbnailActions thumbnailActions = new TrailerAdapter.ThumbnailActions() {
        @Override
        public void onThumbnailClick(String key) {
            Intent intent = new Intent(getContext(), YoutubeActivity.class);
            // COMPLETED: 28/03/2019 A string "key" deve estar numa class para Constants para se tivers que alterar o valor não teres que ir a todas as class que têm esta key*
            intent.putExtra(Settings.KEY, key);
            startActivity(intent);
        }
    };


}