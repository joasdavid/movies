package com.example.form3.dsmovies.mvp.contracts;

/**
 * Created by Joás V. Pereira
 * on 28 Mar. 2019.
 */
public interface BaseViewContractForDataFetching<T> {

    void onStartWork();

    void onEndWork();

    void onSuccess(T successResponse);

    void onFail(Exception e);

}
