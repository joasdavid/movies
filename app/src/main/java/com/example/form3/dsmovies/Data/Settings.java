package com.example.form3.dsmovies.Data;

public class Settings {

    // Program Types
    public static final String PROGRAM_MOVIE = "movie";
    public static final String PROGRAM_TVSHOW = "tvshow";
    public static final String PROGRAM_FAVORITES = "favorites";

    // Base url for get image
    public static final String IMG_BASE_URL = "http://image.tmdb.org/t/p/";
    //"w92", "w154", "w185", "w342", "w500", "w780", or "original
    // Size for images
    public static final String IMG_NORMAL_SIZE = "w185/";
    // Size for thumbnails
    public static final String IMG_THUMBNAIL_SIZE = "original/";

    // Base url for get json
    public static final String JSON_BASE_URL = "http://api.themoviedb.org/3/movie/";
    public static final String JSON_TV_SHOWS_BASE_URL = "http://api.themoviedb.org/3/tv/";

    // Api Key
    public static final String JSON_API_KEY = "efaee4d72801db54c48b8c94a603a87a";

    // Youtube Api Key
    public static final String YOUTUBE_API_KEY = "AIzaSyCH-35ek1-loG3rHqC5b-YGgimTmSpIDX4";


    public static final String KEY = "key";
}