package com.example.form3.dsmovies.repository;

import android.database.Cursor;

import com.example.form3.dsmovies.Data.Settings;
import com.example.form3.dsmovies.mvp.models.TvShows;
import com.example.form3.dsmovies.repository.FetchDataTask.FetchWork;
import com.example.form3.dsmovies.repository.FetchDataTask.FetchWorkStates;
import com.example.form3.dsmovies.repository.database.DBHelper;
import com.example.form3.dsmovies.repository.network.NetworkManager;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daniel Sobral on 03/04/2019.
 */
public class TvShowsRepo {

    private static final String PROGRAM_TYPE = Settings.PROGRAM_TVSHOW;

    private DBHelper dbHelper;
    private NetworkManager networkManager;

    public enum TvShowsType {
        POPULAR("popular"),
        TOP_RATED("top_rated"),
        FAVORITES("favorites");

        final String typeString;

        TvShowsType(String typeString) {
            this.typeString = typeString;
        }

        public static TvShowsType getTypeString(String typeString) {
            for (TvShowsType slt : TvShowsType.values()) {
                if (slt.typeString.equals(typeString)) {
                    return slt;
                }
            }
            return POPULAR;
        }

    }

    public TvShowsRepo(DBHelper dbHelper) {
        this.dbHelper = dbHelper;
        networkManager = NetworkManager.getInstance();
    }

    public void getTvShowsList(TvShowsType type, FetchWorkStates<List<TvShows>> workStates) {
        FetchWork<String, List<TvShows>> work = new FetchWork<String, List<TvShows>>() {
            @Override
            public List<TvShows> onBackgroundWork(String... parm) throws Exception {
                if (TvShowsType.getTypeString(parm[0]) == TvShowsType.FAVORITES) {

                    return favoriteMoviesListUseCase();
                }
                return popularAndTopRatedUseCase(parm[0]);
            }
        };

        new FetchDataTask<>(work, workStates).execute(type.typeString);
    }

    public void getTvShows(String id, FetchWorkStates<TvShows> workStates) {
        FetchWork<String, TvShows> work = new FetchWork<String, TvShows>() {
            @Override
            public TvShows onBackgroundWork(String... parm) throws IOException, JSONException {
                return networkManager.getTvShowsDetails(parm[0], PROGRAM_TYPE);
            }
        };

        new FetchDataTask<>(work, workStates).execute(id);
    }

    private List<TvShows> popularAndTopRatedUseCase(String type) throws IOException, JSONException {
        return networkManager.getTvShowsList(type, PROGRAM_TYPE);
    }

    private List<TvShows> favoriteMoviesListUseCase() {
        List<TvShows> tvShows = new ArrayList<>();
        Cursor cursor = dbHelper.getFavoritesTvShowsList();
        cursor.moveToFirst();

        if (cursor.getCount() > 0) {
            do {
                String id_tvshow = cursor.getString(cursor.getColumnIndex("id_tvshow"));
                String title = cursor.getString(cursor.getColumnIndex("title"));
                String thumbnail = cursor.getString(cursor.getColumnIndex("thumbnail"));
                String synopsis = cursor.getString(cursor.getColumnIndex("synopsis"));

                tvShows.add(new TvShows(id_tvshow, title, thumbnail, synopsis));
            } while (cursor.moveToNext());
        }
        return tvShows;
    }

    public boolean isFavorite(String id) {
        Cursor cursor = dbHelper.getTvShowsFavorite(id);
        cursor.moveToFirst();
        return cursor.getCount() > 0;
    }

    public void setFavorite(TvShows tvShows) {
        String id = tvShows.getId();
        Cursor cursor = dbHelper.getTvShowsFavorite(id);
        cursor.moveToFirst();

        if (cursor.getCount() > 0) {
            dbHelper.deleteTvShowFavorite(id);
        } else {
            dbHelper.insertFavorite(tvShows);
        }
    }
}
