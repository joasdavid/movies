package com.example.form3.dsmovies.repository;

import android.database.Cursor;

import com.example.form3.dsmovies.mvp.models.Movie;
import com.example.form3.dsmovies.repository.FetchDataTask.FetchWork;
import com.example.form3.dsmovies.repository.FetchDataTask.FetchWorkStates;
import com.example.form3.dsmovies.repository.database.DBHelper;
import com.example.form3.dsmovies.repository.network.NetworkManager;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joás V. Pereira
 * on 19 Mar. 2019.
 */
public class MoviesRepo {

    private DBHelper dbHelper;
    private NetworkManager networkManager;

    public enum MoviesListType{
        POPULAR("popular"),
        TOP_RATED("top_rated"),
        FAVORITES("favorites");

        final String typeString;

        MoviesListType(String typeString) {
            this.typeString = typeString;
        }

        public static MoviesListType getTypeString(String typeString) {
            for (MoviesListType tv : MoviesListType.values()) {
                if (tv.typeString.equals(typeString)) {
                    return tv;
                }
            }
            return POPULAR;
        }

    }

    public MoviesRepo(DBHelper dbHelper) {
        this.dbHelper = dbHelper;
        networkManager = NetworkManager.getInstance();
    }

    public void getMovie(String id, FetchWorkStates<Movie> workStates){
        FetchWork<String, Movie> work = new FetchWork<String, Movie>() {
            @Override
            public Movie onBackgroundWork(String... parm) throws IOException, JSONException {
                return networkManager.getMovieDetails(parm[0]);
            }
        } ;

        new FetchDataTask<>(work, workStates).execute(id);
    }

    public void getMoviesList(MoviesListType type, FetchWorkStates<List<Movie>> workStates){
        FetchWork<String, List<Movie>> work = new FetchWork<String, List<Movie>>() {
            @Override
            public List<Movie> onBackgroundWork(String... parm) throws IOException, JSONException {
                if(MoviesListType.getTypeString(parm[0]) == MoviesListType.FAVORITES){
                    return favoriteMoviesListUseCase();
                }
                return popularAndTopRatedUseCase(parm[0]);
            }
        } ;

        new FetchDataTask<>(work, workStates).execute(type.typeString);
    }

    private List<Movie> favoriteMoviesListUseCase(){
        List<Movie> movies = new ArrayList<>();
        Cursor cursor = dbHelper.getFavoritesMovieList();
        cursor.moveToFirst();

        if (cursor.getCount() > 0) {
            do {
                String id_movie = cursor.getString(cursor.getColumnIndex("id_movie"));
                String title = cursor.getString(cursor.getColumnIndex("title"));
                String thumbnail = cursor.getString(cursor.getColumnIndex("thumbnail"));
                String synopsis = cursor.getString(cursor.getColumnIndex("synopsis"));

                movies.add(new Movie(id_movie, title, thumbnail, synopsis));
            } while (cursor.moveToNext());
        }
        return movies;
    }

    private List<Movie> popularAndTopRatedUseCase(String type) throws IOException, JSONException {
        return networkManager.getListMovies(type);
    }

    public boolean isFavorite(String id){
        Cursor cursor = dbHelper.getMovieFavorite(id);
        cursor.moveToFirst();

        return cursor.getCount() > 0;
    }

    public void setFavorite(Movie movie){
        String id = movie.getId();
        Cursor cursor = dbHelper.getMovieFavorite(id);
        cursor.moveToFirst();

        if (cursor.getCount() > 0) {
            dbHelper.deleteMovieFavorite(id);
        } else {
            dbHelper.insertFavorite(movie);
        }
    }
}
