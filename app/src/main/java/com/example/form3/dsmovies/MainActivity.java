package com.example.form3.dsmovies;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.form3.dsmovies.Data.Settings;
import com.example.form3.dsmovies.Data.Utilities;
import com.example.form3.dsmovies.Fragments.FavoritesFragment;
import com.example.form3.dsmovies.Fragments.MovieListFragment;
import com.example.form3.dsmovies.Fragments.TvShowsListFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.menu_nv)
    NavigationView menu_nv;
    @BindView(R.id.main_layout)
    DrawerLayout main_layout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private static final String TYPE_POPULAR = "popular";
    private static final String TYPE_TOP_RATED = "top_rated";
    private static final String TYPE_FAVORITE = "favorites";

    private String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUi();

        menu_nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                setMenuOptions(menuItem);
                return true;
            }
        });

        type = TYPE_POPULAR;
        setTitle("Movies - Popular");

        if (savedInstanceState == null) {
            openFragment(Settings.PROGRAM_MOVIE);
        }
    }

    private void initUi() {
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, main_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        main_layout.addDrawerListener(toggle);
        toggle.syncState();
    }

    // Open Fragment with type of query
    private void openFragment(String program) {
        FragmentManager fragmentManager = MainActivity.this.getSupportFragmentManager();
        Fragment frag = null;

        switch (program) {
            case Settings.PROGRAM_MOVIE:
                frag = MovieListFragment.newInstance(type);
                break;
            case Settings.PROGRAM_TVSHOW:
                frag = TvShowsListFragment.newInstance(type);
                break;
            case Settings.PROGRAM_FAVORITES:
                frag = FavoritesFragment.newInstance(type);
                break;
        }
        if(frag != null)
        Utilities.changeFragmentWithAnimation(frag, fragmentManager);
    }

    @Override
    public void onBackPressed() {
        if (main_layout.isDrawerOpen(menu_nv)) {
            main_layout.closeDrawer(menu_nv);
        } else {
            super.onBackPressed();
        }
    }

    private void setMenuOptions(MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.menu_movie_popular:
                type = TYPE_POPULAR;
                setTitle("Movies - Popular");
                openFragment(Settings.PROGRAM_MOVIE);
                break;

            case R.id.menu_movie_top_rated:
                type = TYPE_TOP_RATED;
                setTitle("Movies - Top Rated");
                openFragment(Settings.PROGRAM_MOVIE);
                break;

            case R.id.menu_tv_shows_popular:
                type = TYPE_POPULAR;
                setTitle("TV Shows - Popular");
                openFragment(Settings.PROGRAM_TVSHOW);
                break;

            case R.id.menu_tv_shows_top_rated:
                type = TYPE_TOP_RATED;
                setTitle("TV Shows - Top Rated");
                openFragment(Settings.PROGRAM_TVSHOW);
                break;

            case R.id.menu_favorites_movies:
                type = TYPE_FAVORITE;
                setTitle("Favorites - Movies");
                openFragment(Settings.PROGRAM_MOVIE);
                break;

            case R.id.menu_favorites_tv_shows:
                type = TYPE_FAVORITE;
                setTitle("Favorites - TV Shows");
                openFragment(Settings.PROGRAM_TVSHOW);
                break;

            case R.id.menu_favorites:
                type = TYPE_FAVORITE;
                setTitle("Favorites");
                openFragment(Settings.PROGRAM_FAVORITES);
                break;

            default:
                type = TYPE_POPULAR;
                setTitle("Movies - Popular");
                openFragment(Settings.PROGRAM_MOVIE);
                break;
        }
        closeMenuDrawer();
    }

    private void closeMenuDrawer() {
        if (main_layout.isDrawerOpen(menu_nv)) {
            main_layout.closeDrawer(menu_nv);
        }
    }
}
