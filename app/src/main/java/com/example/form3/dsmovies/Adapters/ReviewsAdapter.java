package com.example.form3.dsmovies.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.form3.dsmovies.R;
import com.example.form3.dsmovies.mvp.models.Review;

import java.util.List;

public class ReviewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Review> mReview;

    public ReviewsAdapter( List<Review> reviews) {
        this.mReview = reviews;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder viewHolder;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.reviews_list_item, parent, false);
        viewHolder = new ReviewsAdapterViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (position % 2 == 0) {
            ((ReviewsAdapterViewHolder) holder).mListLinearLayout.setBackgroundResource(R.color.colorGrey);
        }

        ((ReviewsAdapterViewHolder) holder).mContentTextView.setText(mReview.get(position).getReview());
        ((ReviewsAdapterViewHolder) holder).mAuthorTextView.setText("by: \"" + mReview.get(position).getAuthor() + "\"");
    }

    @Override
    public int getItemCount() {
        return mReview.size();
    }

    public class ReviewsAdapterViewHolder extends RecyclerView.ViewHolder {

        TextView mContentTextView, mAuthorTextView;
        LinearLayout mListLinearLayout;

        public ReviewsAdapterViewHolder(View itemView) {
            super(itemView);

            // Reviews Layout
            mListLinearLayout = (LinearLayout) itemView.findViewById(R.id.ll_review_list);
            mContentTextView = (TextView) itemView.findViewById(R.id.tv_review_content);
            mAuthorTextView = (TextView) itemView.findViewById(R.id.tv_review_author);
        }
    }
}