package com.example.form3.dsmovies.mvp.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Daniel Sobral on 03/04/2019.
 */
public class Program implements Serializable {

    private String id, title, rate, thumbnail, synopsis;
    protected List<Trailer> trailer;
    protected List<Review> reviews;

    public Program (String id, String title, String thumbnail, String synopsis){
        this.id = id;
        this.title = title;
        this.thumbnail = thumbnail;
        this.synopsis = synopsis;
    }

    public Program(String id, String title, String rate, String thumbnail, String synopsis, List<Trailer> trailer, List<Review> reviews) {
        this.id = id;
        this.title = title;
        this.rate = rate;
        this.thumbnail = thumbnail;
        this.synopsis = synopsis;
        this.trailer = trailer;
        this.reviews = reviews;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public List<Trailer> getTrailer() {
        return trailer;
    }

    public void setTrailer(List<Trailer> trailer) {
        this.trailer = trailer;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }
}
