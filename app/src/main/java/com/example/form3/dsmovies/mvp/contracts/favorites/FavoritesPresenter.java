package com.example.form3.dsmovies.mvp.contracts.favorites;

import com.example.form3.dsmovies.Data.Settings;
import com.example.form3.dsmovies.mvp.base.BasePresenter;
import com.example.form3.dsmovies.mvp.contracts.favorites.FavoritesContract.FavoritesContractPresenter;
import com.example.form3.dsmovies.mvp.contracts.favorites.FavoritesContract.FavoritesContractView;
import com.example.form3.dsmovies.mvp.models.Movie;
import com.example.form3.dsmovies.mvp.models.TvShows;
import com.example.form3.dsmovies.repository.FetchDataTask.FetchWorkStates;
import com.example.form3.dsmovies.repository.MoviesRepo;
import com.example.form3.dsmovies.repository.TvShowsRepo;

import java.util.List;

/**
 * Created by Daniel Sobral on 04/04/2019.
 */
public class FavoritesPresenter extends BasePresenter<FavoritesContractView> implements
        FavoritesContractPresenter {

    private MoviesRepo moviesRepo;
    private TvShowsRepo tvShowsRepo;

    private List<Movie> movieList;
    private List<TvShows> tvShowsList;

    private Exception exceptionMovie;
    private Exception exceptionTvShow;

    public FavoritesPresenter(MoviesRepo moviesRepo, TvShowsRepo tvShowsRepo) {
        this.moviesRepo = moviesRepo;
        this.tvShowsRepo = tvShowsRepo;
    }

    private FetchWorkStates<List<Movie>> statesMovie = new FetchWorkStates<List<Movie>>() {
        @Override
        public void onStartWork() {

        }

        @Override
        public void onEndWork() {

        }

        @Override
        public void onSuccess(List<Movie> movies) {
            movieList = movies;
            onSuccessFinish();
        }

        @Override
        public void onFail(Exception e) {
            exceptionMovie = e;
            onFailFinish();
        }
    };

    private FetchWorkStates<List<TvShows>> statesTvShows = new FetchWorkStates<List<TvShows>>() {
        @Override
        public void onStartWork() {

        }

        @Override
        public void onEndWork() {

        }

        @Override
        public void onSuccess(List<TvShows> tvShows) {
            tvShowsList = tvShows;
            onSuccessFinish();
        }

        @Override
        public void onFail(Exception e) {
            exceptionTvShow = e;
            onFailFinish();
        }
    };

    private void onSuccessFinish(){
        if(movieList != null && tvShowsList != null){
            getViewState().onSuccess(movieList, tvShowsList);
        }
    }

    private void onFailFinish(){
        if(exceptionMovie == null && exceptionTvShow == null){
            getViewState().onFail(new Exception(String.valueOf(exceptionMovie) + " " + String.valueOf(exceptionTvShow)));
        }
    }

    @Override
    public void getFavoritesListByType(String type) {
        moviesRepo.getMoviesList(MoviesRepo.MoviesListType.getTypeString(type), statesMovie);
        tvShowsRepo.getTvShowsList(TvShowsRepo.TvShowsType.getTypeString(type), statesTvShows);
    }

    @Override
    public void checkFavoriteList(String id, String type) {
        boolean isFavorite;

        if (type.equals(Settings.PROGRAM_MOVIE)) {
            isFavorite = moviesRepo.isFavorite(id);
        } else {
            isFavorite = tvShowsRepo.isFavorite(id);
        }

        getViewState().favouriteState(isFavorite);
    }

    @Override
    public void actionFavorite(Movie movie) {
        moviesRepo.setFavorite(movie);
        checkFavoriteList(movie.getId(), Settings.PROGRAM_MOVIE);
    }

    @Override
    public void actionFavorite(TvShows tvShows) {
        tvShowsRepo.setFavorite(tvShows);
        checkFavoriteList(tvShows.getId(), Settings.PROGRAM_TVSHOW);
    }

}
