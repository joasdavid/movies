package com.example.form3.dsmovies.repository.network;

import android.net.Uri;

import com.example.form3.dsmovies.Data.Settings;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class NetworkUtilities {

    final static String QUERY_PARAM = "api_key";

    public static URL buildUrl(String type) {

        String jsonUrl = Settings.JSON_BASE_URL + type;

        Uri builtUri = Uri.parse(jsonUrl).buildUpon()
                //.appendQueryParameter("page", page)
                .appendQueryParameter(QUERY_PARAM, Settings.JSON_API_KEY)
                .build();

        URL url = null;

        try {
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return url;
    }

    public static URL buildUrl(String type, String program) {

        String jsonUrl;

        if(program == Settings.PROGRAM_MOVIE){
            jsonUrl = Settings.JSON_BASE_URL + type;
        } else {
            jsonUrl = Settings.JSON_TV_SHOWS_BASE_URL + type;
        }


        Uri builtUri = Uri.parse(jsonUrl).buildUpon()
                //.appendQueryParameter("page", page)
                .appendQueryParameter(QUERY_PARAM, Settings.JSON_API_KEY)
                .build();

        URL url = null;

        try {
            url = new URL(builtUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return url;
    }

    public static String getResponseFromHttpUrl(URL url) throws IOException {

        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

        try {
            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();

            if (hasInput) {
                return scanner.next();
            } else {
                return null;
            }
        } finally {
            urlConnection.disconnect();
        }
    }
}