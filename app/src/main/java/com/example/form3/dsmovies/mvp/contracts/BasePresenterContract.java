package com.example.form3.dsmovies.mvp.contracts;

/**
 * Created by Joás V. Pereira
 * on 28 Mar. 2019.
 */
public interface BasePresenterContract<T> {

  void bindView(T view);

}
