package com.example.form3.dsmovies.viewgroup;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.form3.dsmovies.R;

/**
 * Created by Daniel Sobral on 29/03/2019.
 */
public class CustomBarViewGroup extends ConstraintLayout {

    private Context context;
    private AttributeSet attrs;
    private String title;
    private int titleColor;
    private int titleFont;
    private TextView mTitle;
    private ImageView mAction;

    private int actionOpen;
    private int actionClose;

    private boolean isShowing = false;

    public CustomBarViewGroup(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public CustomBarViewGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.attrs = attrs;
        init();
    }

    public CustomBarViewGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.attrs = attrs;
        init();
    }

    private void init() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.custom_viewgroup, this, true);
        LayoutInflater.from(context).inflate(R.layout.custom_viewgroup, this);

        getAttributesFromXmlAndStoreLocally(attrs);

        mTitle = findViewById(R.id.tv_title);
        mAction = findViewById(R.id.iv_action);

        mTitle.setText(title);
        mTitle.setTextColor(titleColor);

        setAction(isShowing);

        mAction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setShowing(!isShowing);
            }
        });

    }

    private void getAttributesFromXmlAndStoreLocally(AttributeSet attrs) {
        TypedArray attributesFromXmlLayout = getContext()
                .obtainStyledAttributes(attrs, R.styleable.CustomBarViewGroup);
        if (attributesFromXmlLayout == null) {
            return;
        }

        title = attributesFromXmlLayout.getString(R.styleable.CustomBarViewGroup_titleCustomBar);
        titleColor = attributesFromXmlLayout.getInt(R.styleable.CustomBarViewGroup_titleColorCustomBar, R.color.colorGrey);
        actionOpen = attributesFromXmlLayout.getResourceId(R.styleable.CustomBarViewGroup_imageOpenCustomBar, R.mipmap.ic_arrow_up);
        actionClose = attributesFromXmlLayout.getResourceId(R.styleable.CustomBarViewGroup_imageCloseCustomBar, R.mipmap.ic_arrow_down);

        attributesFromXmlLayout.recycle();
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        // TODO: 02/04/2019 (1) Coloca tudo o que esteja após a primeira linha a Gone 
        if (getChildCount() != 0) {
            child.setVisibility(GONE);
        }
        super.addView(child, index, params);
    }

    private void setAction(boolean action) {
        if (action) {
            mAction.setImageResource(actionOpen);
        } else {
            mAction.setImageResource(actionClose);
        }
    }

    public void setTitle(String text) {
        this.mTitle.setText(text);
    }

    // TODO: 02/04/2019 (2)
    public void setShowing(boolean showing) {
        isShowing = showing;
        for (int i = 1; i < getChildCount(); i++) {
            if (showing) {
                getChildAt(i).setVisibility(VISIBLE);
            } else {
                getChildAt(i).setVisibility(GONE);
            }
        }
        setAction(showing);
    }

}
