package com.example.form3.dsmovies.mvp.contracts.movies.list;

import com.example.form3.dsmovies.mvp.base.BasePresenter;
import com.example.form3.dsmovies.mvp.contracts.movies.list.MoviesListContract.MoviesListContractPresenter;
import com.example.form3.dsmovies.mvp.contracts.movies.list.MoviesListContract.MoviesListContractView;
import com.example.form3.dsmovies.mvp.models.Movie;
import com.example.form3.dsmovies.repository.FetchDataTask.FetchWorkStates;
import com.example.form3.dsmovies.repository.MoviesRepo;
import com.example.form3.dsmovies.repository.MoviesRepo.MoviesListType;

import java.util.List;

/**
 * Created by Joás V. Pereira
 * on 28 Mar. 2019.
 */
public class MoviesListPresenter extends BasePresenter<MoviesListContractView> implements
        MoviesListContractPresenter,
        FetchWorkStates<List<Movie>> {

    MoviesRepo moviesRepo;

    public MoviesListPresenter(MoviesRepo moviesRepo) {
        this.moviesRepo = moviesRepo;
    }

    @Override
    public void getListOfMoviesByType(String type) {
        moviesRepo.getMoviesList(MoviesListType.getTypeString(type), this);
    }

    // TODO: 02/04/2019 (5) Duplicação das funções
    @Override
    public void checkFavoriteList(String id) {
        boolean isFavorite;

        isFavorite = moviesRepo.isFavorite(id);

        getViewState().favouriteState(isFavorite);
    }

    // TODO: 02/04/2019 (6) Duplicação das funções
    @Override
    public void actionFavorite(Movie movie) {
        moviesRepo.setFavorite(movie);
        checkFavoriteList(movie.getId());
    }

    @Override
    public void onStartWork() {
        //void
    }

    @Override
    public void onEndWork() {
        //void
    }

    @Override
    public void onSuccess(List<Movie> movies) {
        getViewState().onSuccess(movies);
    }

    @Override
    public void onFail(Exception e) {
        getViewState().onFail(e);
    }
}
