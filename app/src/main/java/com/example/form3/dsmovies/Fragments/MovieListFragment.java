package com.example.form3.dsmovies.Fragments;

import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.form3.dsmovies.Adapters.MovieAdapter;
import com.example.form3.dsmovies.Adapters.MovieAdapter.ImageActions;
import com.example.form3.dsmovies.Data.Utilities;
import com.example.form3.dsmovies.MoviesApp;
import com.example.form3.dsmovies.R;
import com.example.form3.dsmovies.mvp.contracts.movies.list.MoviesListContract.MoviesListContractPresenter;
import com.example.form3.dsmovies.mvp.contracts.movies.list.MoviesListContract.MoviesListContractView;
import com.example.form3.dsmovies.mvp.contracts.movies.list.MoviesListPresenter;
import com.example.form3.dsmovies.mvp.models.Movie;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieListFragment extends BaseFragment implements MoviesListContractView {

    @BindView(R.id.rv_movie_list) RecyclerView mRecyclerView;
    @BindView(R.id.info_bottom_sheet) View infoBottomSheet;
    @BindView(R.id.title_tv) TextView titleTextView;
    @BindView(R.id.close_tv) TextView closeTextView;
    @BindView(R.id.show_details_tv) TextView showDetailsTextView;
    @BindView(R.id.action_favorites_tv) TextView actionFavoritesTextView;


    private static final int SPAN_COUNT_RECYCLER_VIEW = 2;
    public static String TAG = MovieListFragment.class.getName();

    private View rootView;

    private ImageView favImageView;

    private MoviesListContractPresenter presenter;

    private BottomSheetBehavior bottomSheetBehavior;

    public static String TYPEKEY = "TYPEKEY";
    private String type;

    public static MovieListFragment newInstance(String type) {

        Bundle args = new Bundle();

        args.putString(TYPEKEY, type);

        MovieListFragment fragment = new MovieListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        rootView = getActivity().getLayoutInflater()
                .inflate(R.layout.fragment_movies_list, container, false);

        initVarWithArgumentsFromBundle();
        initPresenter();
        initUi();

        return rootView;
    }

    private void initUi() {
        ButterKnife.bind(this, rootView);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), SPAN_COUNT_RECYCLER_VIEW));
        bottomSheetBehavior = BottomSheetBehavior.from(infoBottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }

    private void initVarWithArgumentsFromBundle() {
        type = getArguments().getString(TYPEKEY);
    }

    private void initPresenter() {
        presenter = new MoviesListPresenter(MoviesApp.getInstance().getMoviesRepo());
        presenter.bindView(this);
        showLoading();
        presenter.getListOfMoviesByType(type);
    }

    private MovieAdapter.ImageActions imageClick() {
        return new ImageActions() {
            @Override
            public void onImageClick(final Movie movie, final ImageView fav) {
                bottomSheetBehavior.setState(BottomSheetBehavior. STATE_COLLAPSED);

                checkFavorite(movie.getId(), fav);
                titleTextView.setText(movie.getTitle());
                closeTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bottomSheetBehavior.setState(BottomSheetBehavior. STATE_HIDDEN);
                    }
                });

                showDetailsTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDetails(movie.getId());
                    }
                });

                actionFavoritesTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        actionFavorite(movie, fav);
                    }
                });
            }

            // TODO: 02/04/2019 (8) Override dos metedos para implementar função dos Favoritos
            @Override
            public void checkFavorite(String id, ImageView fav) {
                favImageView = fav;
                presenter.checkFavoriteList(id);
            }

            @Override
            public void actionFavorite(Movie movie, ImageView fav) {
                favImageView = fav;
                presenter.actionFavorite(movie);
            }
        };
    }

    @Override
    public void onSuccess(List<Movie> movies) {
        hideLoading();
        if (movies != null) {
            MovieAdapter mMovieAdapter = new MovieAdapter(movies, imageClick());
            mRecyclerView.setAdapter(mMovieAdapter);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            Toast.makeText(getContext(), "Can not get data, try again later!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFail(Exception e) {
        hideLoading();
        Toast.makeText(getContext(), "Ups something is broken :s", Toast.LENGTH_LONG).show();
    }

    // TODO: 02/04/2019 (7) Alteração da imagem (Acho que está mal implementado)
    @Override
    public void favouriteState(boolean isFavorite) {
        if(isFavorite && favImageView != null){
            favImageView.setImageResource(R.mipmap.star_del);
            actionFavoritesTextView.setText(R.string.remove_from_favorites);
        } else if (!isFavorite && favImageView != null){
            favImageView.setImageResource(R.mipmap.star_add);
            actionFavoritesTextView.setText(R.string.add_to_favorites);
        }
    }

    private void showDetails (String id){
        String backStack = null;
        Fragment frag = MovieDetatilsFragment.newInstance(id);
        @SuppressWarnings("ConstantConditions")
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        Utilities.changeFragmentBackStackWithAnimation(frag,fragmentManager, backStack);
    }
}